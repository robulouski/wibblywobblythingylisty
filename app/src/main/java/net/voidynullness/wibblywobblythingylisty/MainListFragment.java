package net.voidynullness.wibblywobblythingylisty;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainListFragment extends ListFragment {
    private OnOptionSelectedListener mListener;

    public static final String [] MAIN_LIST_OPTIONS = {
            "ListActivity",
            "ListFragment",
            "Headings List",
    };
    public static final int IDX_MAIN_LIST_LISTACTIVITY = 0;
    public static final int IDX_MAIN_LIST_LISTFRAGMENT = 1;
    public static final int IDX_MAIN_LIST_HEADINGS = 2;

    public MainListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setListAdapter(new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                MAIN_LIST_OPTIONS));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnOptionSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnMainListOptionSelectListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        if (null != mListener) {
            mListener.onOptionSelected(position);
        }
    }
    public interface OnOptionSelectedListener {
        public void onOptionSelected(int id);
    }
}
